document.onreadystatechange = function(state){

    if(document.readyState == "complete"){

        var comment = document.querySelectorAll('button.comment')[0],
            commentOpen = false,
            message = document.querySelectorAll('.message_container')[0];

        if(comment){

            comment.addEventListener('click', function(elem){
                commentOpen = !commentOpen;

                if(commentOpen){
                    comment.className = 'icon-button comment active';
                    message.className = 'message_container opened';
                }else{
                    comment.className = 'icon-button comment';
                    message.className = 'message_container';
                }

            }, true);
        }

    }


};